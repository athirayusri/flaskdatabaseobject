from sqlite3 import Error
import os
import sqlite3


class Database:
    conn = None

    def __init__(self):
        self.conn = sqlite3.connect('data.db', check_same_thread=False)
        self.cur = self.conn.cursor()

    def update(self, id):
        self.cur = self.conn.cursor()
        self.cur.execute('SELECT * from data WHERE id = ?', (id,))
        record = cur.fetchone()
        self.conn.commit()

    def updatedata(self, id, first_name, last_name, email, gender, ip_address):
        self.cur = self.conn.cursor()        
        self.cur.execute('UPDATE data SET first_name = ?, last_name = ?, email = ?, gender = ?, ip_address = ? WHERE id = ?', (first_name, last_name, email, gender, ip_address,id,))
        self.conn.commit()

    def delete(self, id):
        self.cur = self.conn.cursor()        
        self.cur.execute('DELETE from data WHERE id = ?', (id,))
        self.conn.commit()
    
    def view(self, id):
        self.cur = self.conn.cursor()  
        self.conn.row_factory = sqlite3.Row      
        self.cur.execute('SELECT * from data WHERE id = ?', (id,))
        return self.cur.fetchone()
    
    def list(self):
        self.cur = self.conn.cursor()  
        self.conn.row_factory = sqlite3.Row      
        self.cur.execute('SELECT * from data')
        rows = self.cur.fetchall()
        return rows       

    def disconnect(self):
        self.conn.close()
