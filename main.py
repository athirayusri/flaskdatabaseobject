from flask import Flask, make_response, render_template, request, redirect
import io
import csv
import os
import sqlite3
import database
from sqlite3 import Error
from database import Database

app = Flask(__name__)
db = Database()
#db.view()

@app.route('/')
def index():
     return render_template('index.html')

@app.route('/view')
def view():
    db.list()
    rows = db.list()
    return render_template('view.html', rows = rows)

@app.route('/update/<int:id>', methods = ['GET'])
def update(id):
    #id = request.form.get('id')
    record = db.view(id)
    #db.view()
    return render_template('update.html', record = record)

@app.route('/updatedata/<int:id>', methods = ['POST'])
def updatedata(id):
    #id = request.form.get('id')
    first_name = request.form.get('first_name')
    last_name = request.form.get('last_name')
    email = request.form.get('email')
    gender = request.form.get('gender')
    ip_address = request.form.get('ip_address')
    db.updatedata (id, first_name,last_name, email, gender, ip_address)
    return redirect('/view')

@app.route('/delete/<int:id>')
def delete(id):
    #id = request.form.get('id')
    db.delete(id)
    return redirect('/view')

if __name__ == '__main__':
    app.run(debug=True)
