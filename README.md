# Flask Database

This program is to display SQL database using Flask and HTML.

## Prerequisites
Flask and sqlite3 needs to be installed beforehand. 

## Running
There are two Python programs, main.py and database.py. Whereby, main.py is the main program to display the database. While database.py contains the functions of the database.  This is to ensure that there will be no connecting to sqlite3 redundancy. There are also four HTML templates in order for the database to be displayed correctly. 

### Python Programs
#### main.py
Calls for different functions from database.py according to the user needs.

#### database.py
Stores display, delete and update functions of database.

### HTML Templates
#### index.html
Landing page when main.py is run.
#### view.html
Displaying database in table form.
#### delete.html
Deleting data from database.
#### update.html
Update form when updating a data in database.